# Custom environment with instrinsic reward
from mani_skill2.envs.pick_and_place.stack_cube import StackCubeEnv
from mani_skill2.utils.registration import register_env
import numpy as np
import torch

@register_env("StackCube-v4", max_episode_steps=200)
class StackCubeCuriosityEnv(StackCubeEnv):
    def __init__(self, curiosity_type, *args, curiosity_model = None, **kwargs):
        self.curiosity_type = curiosity_type
        if curiosity_type != "":
            print(f"Using {curiosity_type} for curiosity")
        else:
            print("Non-curious agent")
        if curiosity_model is None:
            print("No curiosity model is loaded")
        else:
            print(f"{curiosity_type} model is loaded")

        self.curiosity_model = curiosity_model
        super().__init__(*args, **kwargs)
        self.prev_obs = None
    
    
    def compute_dense_reward(self, info, **kwargs):
        reward = self.get_extrinsic_reward(info)
        if self.prev_obs is not None:
            reward += 0.3 * self.get_intrinsic_reward(info)
        self.prev_obs = self.get_obs()
        return reward
    
    
    def get_intrinsic_reward(self, info):
        if self.curiosity_type == "RND":
            curr_obs = self.get_obs()
            curr_obs = torch.from_numpy(curr_obs).float().to(self.curiosity_model.device)
            target_feature = self.curiosity_model(self.prev_obs)
            intrinsic_reward = torch.linalg.norm(target_feature.flatten() - curr_obs.flatten())
            intrinsic_reward = intrinsic_reward.detach().cpu().numpy()
            return intrinsic_reward
        elif self.curiosity_type == "ICM":
            curr_obs = self.get_obs()
            curr_obs = torch.from_numpy(curr_obs).float().to(self.RND_model.device)   
            target_feature = self.curiosity_model(self.prev_obs)
            intrinsic_reward = torch.linalg.norm(target_feature.flatten() - curr_obs.flatten())
            intrinsic_reward = intrinsic_reward.detach().cpu().numpy()
            return intrinsic_reward
        else:
            return 0.0

    def get_extrinsic_reward(self, info):
        gripper_width = (
            self.agent.robot.get_qlimits()[-1, 1] * 2
        )  # NOTE: hard-coded with panda
        reward = 0.0

        if info["success"]:
            reward = 15.0
        else:
            if info["elapsed_steps"] > 1_000_000:
                # grasp pose rotation reward
                grasp_rot_loss_fxn = lambda A: np.tanh(
                    1 / 8 * np.trace(A.T @ A)
                )  # trace(A.T @ A) has range [0,8] for A being difference of rotation matrices
                tcp_pose_wrt_cubeA = self.cubeA.pose.inv() * self.tcp.pose
                tcp_rot_wrt_cubeA = tcp_pose_wrt_cubeA.to_transformation_matrix()[:3, :3]
                gt_rots = [
                    np.array([[0, 1, 0], [1, 0, 0], [0, 0, -1]]),
                    np.array([[0, -1, 0], [-1, 0, 0], [0, 0, -1]]),
                    np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]]),
                    np.array([[-1, 0, 0], [0, 1, 0], [0, 0, -1]]),
                ]
                grasp_rot_loss = min(
                    [grasp_rot_loss_fxn(x - tcp_rot_wrt_cubeA) for x in gt_rots]
                )
                reward += 1 - grasp_rot_loss

            if info["elapsed_steps"] > 400_000:
                cubeB_vel_penalty = np.linalg.norm(self.cubeB.velocity) + np.linalg.norm(
                    self.cubeB.angular_velocity
                )
                reward -= cubeB_vel_penalty

            # reaching object reward
            tcp_pose = self.tcp.pose.p
            cubeA_pos = self.cubeA.pose.p
            cubeA_to_tcp_dist = np.linalg.norm(tcp_pose - cubeA_pos)
            reaching_reward = 1 - np.tanh(3.0 * cubeA_to_tcp_dist)
            reward += reaching_reward

            # check if cubeA is on cubeB
            cubeA_pos = self.cubeA.pose.p
            cubeB_pos = self.cubeB.pose.p
            goal_xyz = np.hstack(
                [cubeB_pos[0:2], cubeB_pos[2] + self.box_half_size[2] * 2]
            )
            cubeA_on_cubeB = (
                np.linalg.norm(goal_xyz[:2] - cubeA_pos[:2])
                < self.box_half_size[0] * 0.8
            )
            cubeA_on_cubeB = cubeA_on_cubeB and (
                np.abs(goal_xyz[2] - cubeA_pos[2]) <= 0.005
            )
            if cubeA_on_cubeB:
                reward = 10.0
                # ungrasp reward
                is_cubeA_grasped = self.agent.check_grasp(self.cubeA)
                if not is_cubeA_grasped:
                    reward += 2.0
                else:
                    reward = (
                        reward
                        + 2.0 * np.sum(self.agent.robot.get_qpos()[-2:]) / gripper_width
                    )
            else:
                # grasping reward
                is_cubeA_grasped = self.agent.check_grasp(self.cubeA)
                if is_cubeA_grasped:
                    reward += 1.0

                # reaching goal reward, ensuring that cubeA has appropriate height during this process
                if is_cubeA_grasped:
                    cubeA_to_goal = goal_xyz - cubeA_pos
                    # cubeA_to_goal_xy_dist = np.linalg.norm(cubeA_to_goal[:2])
                    cubeA_to_goal_dist = np.linalg.norm(cubeA_to_goal)
                    appropriate_height_penalty = np.maximum(
                        np.maximum(2 * cubeA_to_goal[2], 0.0),
                        np.maximum(2 * (-0.02 - cubeA_to_goal[2]), 0.0),
                    )
                    reaching_reward2 = 2 * (
                        1 - np.tanh(5.0 * appropriate_height_penalty)
                    )
                    # qvel_penalty = np.sum(np.abs(self.agent.robot.get_qvel())) # prevent the robot arm from moving too fast
                    # reaching_reward2 -= 0.0003 * qvel_penalty
                    # if appropriate_height_penalty < 0.01:
                    reaching_reward2 += 4 * (1 - np.tanh(5.0 * cubeA_to_goal_dist))
                    reward += np.maximum(reaching_reward2, 0.0)


@register_env("StackCube-v2", max_episode_steps=200)
class StackCubeCustomRewardEnv(StackCubeEnv):
    def compute_dense_reward(self, info, **kwargs):
        gripper_width = (
            self.agent.robot.get_qlimits()[-1, 1] * 2
        )  # NOTE: hard-coded with panda
        reward = 0.0

        if info["success"]:
            reward = 15.0
        else:
            if info["elapsed_steps"] > 1_000_000:
                # grasp pose rotation reward
                grasp_rot_loss_fxn = lambda A: np.tanh(
                    1 / 8 * np.trace(A.T @ A)
                )  # trace(A.T @ A) has range [0,8] for A being difference of rotation matrices
                tcp_pose_wrt_cubeA = self.cubeA.pose.inv() * self.tcp.pose
                tcp_rot_wrt_cubeA = tcp_pose_wrt_cubeA.to_transformation_matrix()[:3, :3]
                gt_rots = [
                    np.array([[0, 1, 0], [1, 0, 0], [0, 0, -1]]),
                    np.array([[0, -1, 0], [-1, 0, 0], [0, 0, -1]]),
                    np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]]),
                    np.array([[-1, 0, 0], [0, 1, 0], [0, 0, -1]]),
                ]
                grasp_rot_loss = min(
                    [grasp_rot_loss_fxn(x - tcp_rot_wrt_cubeA) for x in gt_rots]
                )
                reward += 1 - grasp_rot_loss

            if info["elapsed_steps"] > 400_000:
                cubeB_vel_penalty = np.linalg.norm(self.cubeB.velocity) + np.linalg.norm(
                    self.cubeB.angular_velocity
                )
                reward -= cubeB_vel_penalty

            # reaching object reward
            tcp_pose = self.tcp.pose.p
            cubeA_pos = self.cubeA.pose.p
            cubeA_to_tcp_dist = np.linalg.norm(tcp_pose - cubeA_pos)
            reaching_reward = 1 - np.tanh(3.0 * cubeA_to_tcp_dist)
            reward += reaching_reward

            # check if cubeA is on cubeB
            cubeA_pos = self.cubeA.pose.p
            cubeB_pos = self.cubeB.pose.p
            goal_xyz = np.hstack(
                [cubeB_pos[0:2], cubeB_pos[2] + self.box_half_size[2] * 2]
            )
            cubeA_on_cubeB = (
                np.linalg.norm(goal_xyz[:2] - cubeA_pos[:2])
                < self.box_half_size[0] * 0.8
            )
            cubeA_on_cubeB = cubeA_on_cubeB and (
                np.abs(goal_xyz[2] - cubeA_pos[2]) <= 0.005
            )
            if cubeA_on_cubeB:
                reward = 10.0
                # ungrasp reward
                is_cubeA_grasped = self.agent.check_grasp(self.cubeA)
                if not is_cubeA_grasped:
                    reward += 2.0
                else:
                    reward = (
                        reward
                        + 2.0 * np.sum(self.agent.robot.get_qpos()[-2:]) / gripper_width
                    )
            else:
                # grasping reward
                is_cubeA_grasped = self.agent.check_grasp(self.cubeA)
                if is_cubeA_grasped:
                    reward += 1.0

                # reaching goal reward, ensuring that cubeA has appropriate height during this process
                if is_cubeA_grasped:
                    cubeA_to_goal = goal_xyz - cubeA_pos
                    # cubeA_to_goal_xy_dist = np.linalg.norm(cubeA_to_goal[:2])
                    cubeA_to_goal_dist = np.linalg.norm(cubeA_to_goal)
                    appropriate_height_penalty = np.maximum(
                        np.maximum(2 * cubeA_to_goal[2], 0.0),
                        np.maximum(2 * (-0.02 - cubeA_to_goal[2]), 0.0),
                    )
                    reaching_reward2 = 2 * (
                        1 - np.tanh(5.0 * appropriate_height_penalty)
                    )
                    # qvel_penalty = np.sum(np.abs(self.agent.robot.get_qvel())) # prevent the robot arm from moving too fast
                    # reaching_reward2 -= 0.0003 * qvel_penalty
                    # if appropriate_height_penalty < 0.01:
                    reaching_reward2 += 4 * (1 - np.tanh(5.0 * cubeA_to_goal_dist))
                    reward += np.maximum(reaching_reward2, 0.0)

        return reward
