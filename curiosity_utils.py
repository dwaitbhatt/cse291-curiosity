import numpy as np
from stable_baselines3.common.callbacks import BaseCallback
import torch
import torch.nn as nn
from torch.nn import init


class RNDCallback(BaseCallback):
    """
    A custom callback that derives from ``BaseCallback``.

    :param verbose: Verbosity level: 0 for no output, 1 for info messages, 2 for debug messages
    """
    def __init__(self, verbose = 0, rnd_model = None, batch_size = 100, n_epochs = 10, update_proportion = 0.2):
        super().__init__(verbose)
        # Those variables will be accessible in the callback
        # (they are defined in the base class)
        # The RL model
        # self.model = None  # type: BaseRLModel
        # An alias for self.model.get_env(), the environment used for training
        # self.training_env = None  # type: Union[gym.Env, VecEnv, None]
        # Number of time the callback was called
        # self.n_calls = 0  # type: int
        # self.num_timesteps = 0  # type: int
        # local and global variables
        # self.locals = None  # type: Dict[str, Any]
        # self.globals = None  # type: Dict[str, Any]
        # The logger object, used to report things in the terminal
        # self.logger = None  # type: logger.Logger
        # # Sometimes, for event callback, it is useful
        # # to have access to the parent object
        # self.parent = None  # type: Optional[BaseCallback]
        self.rnd_model = rnd_model
        self.forward_mse = nn.MSELoss(reduction='none')
        self.update_proportion = 0.2
        self.device = torch.device('cpu') #if torch.cuda.is_available() else torch.device('cpu')
        self.n_epochs = n_epochs
        self.batch_size = batch_size
        self.update_proportion = update_proportion


    def _update_rnd_model(self, obs):
        predict_next_state_feature, target_next_state_feature = self.rnd_model(obs)
        forward_loss = self.forward_mse(predict_next_state_feature, target_next_state_feature.detach()).mean(-1)
        mask = torch.rand(len(forward_loss)).to(self.device)
        mask = (mask < self.update_proportion).type(torch.FloatTensor).to(self.device)
        forward_loss = (forward_loss * mask).sum() / torch.max(mask.sum(), torch.Tensor([1]).to(self.device))
        
        # Backpropagate
        self.rnd_model.optimizer.zero_grad()
        forward_loss.backward()
        nn.utils.clip_grad_norm_(self.rnd_model.parameters(), 10)
        self.rnd_model.optimizer.step()

        return forward_loss.item()


    def _on_rollout_end(self, **kwargs) -> None:
        """
        This event is triggered before updating the policy.
        """
        obs = self.locals["rollout_buffer"].__dict__["observations"]
        # print("obs shape: ", obs.shape)
        obs = torch.from_numpy(obs).float().to(self.device)
        obs = obs.view(-1, obs.shape[2])
        
        rnd_loss = 0
        sample_range = np.arange(len(obs))
        for _ in range(self.n_epochs):
            np.random.shuffle(sample_range)
            for j in range(int(len(obs) / self.batch_size)):
                sample_idx = sample_range[self.batch_size * j : self.batch_size * (j + 1)]
                rnd_loss += self._update_rnd_model(obs[sample_idx])
        self.logger.record("train/RND_loss", rnd_loss/self.n_epochs)


    def _on_step(self) -> bool:
        """
        This event is triggered before updating the policy.
        """
        return True

# class Flatten(nn.Module):
#     def forward(self, input):
#         return input.view(input.size(0), -1)

class RNDModel(nn.Module):
    def __init__(self, input_size, output_size):
        super(RNDModel, self).__init__()

        self.input_size = input_size
        self.output_size = output_size

        self.predictor = nn.Sequential(
            nn.Linear(input_size, 32),
            nn.ReLU(),
            nn.Linear(32, 32),
            nn.ReLU(),
            nn.Linear(32, output_size)
        )

        self.target = nn.Sequential(
            nn.Linear(input_size, 32),
            nn.ReLU(),
            nn.Linear(32, 32),
            nn.ReLU(),
            nn.Linear(32, output_size)
        )

        for p in self.modules():
            if isinstance(p, nn.Conv2d):
                init.orthogonal_(p.weight, np.sqrt(2))
                p.bias.data.zero_()

            if isinstance(p, nn.Linear):
                init.orthogonal_(p.weight, np.sqrt(2))
                p.bias.data.zero_()

        for param in self.target.parameters():
            param.requires_grad = False
        
        self.optimizer = torch.optim.Adam(self.predictor.parameters(), lr=1e-4)
        self.device = torch.device('cpu')# if torch.cuda.is_available() else torch.device('cpu')


    def forward(self, next_obs):
        target_feature = self.target(next_obs)
        predict_feature = self.predictor(next_obs)

        return predict_feature, target_feature